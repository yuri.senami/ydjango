from django.urls import path
from .import views

app_name='scratch'

urlpatterns=[
     path('', views.top, name='top'),
     path('form/', views.form, name='form'),
     path('scratch/', views.sc_list, name='sc_list'),
     path('intro/', views.in_list, name='in_list'),
     path('scratch/nakano/', views.sc_nakano, name='sc_nakano'), #<int:pk>とか使って簡略化できるかも？
     path('scratch/sato/', views.sc_sato, name='sc_sato'),
     path('scratch/senami/', views.sc_senami, name='sc_senami'),
     path('intro/nakano/', views.in_nakano, name='in_nakano'),
     path('intro/sato/', views.in_sato, name='in_sato'),
     path('intro/senami/', views.in_senami, name='in_senami'),
     path('qa/', views.qa, name='qa'), 
]

 