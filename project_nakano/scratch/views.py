from django.shortcuts import render

# Create your views here.
def top(request):
    return render(request,'scratch/base_senami.html')

def form(request):
    return render(request,'scratch/form.html')

def sc_list(request):
    return render(request,'scratch/sc_list.html')

def in_list(request):
    return render(request,'scratch/in_list.html')

def sc_nakano(request):
    return render(request,'scratch/sc_nakano.html')
    
def sc_sato(request):
    return render(request,'scratch/sc_sato.html')

def sc_senami(request):
    return render(request,'scratch/sc_senami.html')


def in_nakano(request):
    return render(request,'scratch/in_nakano.html')
    
def in_sato(request):
    return render(request,'scratch/in_sato.html')

def in_senami(request):
    return render(request,'scratch/in_senami.html')


def qa(request):
   return render(request,'scratch/qa.html')