from django.shortcuts import render,redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from .forms import DayCreateForm
from .models import Day

class IndexView(generic.ListView):
    model = Day
    paginate_by = 10

class AddView(generic.CreateView):
    model = Day
    form_class = DayCreateForm
    #fields='__all__' form_classのかわり 
    success_url = reverse_lazy('diary:index')

class UpdateView(generic.UpdateView):
    model = Day
    form_class = DayCreateForm
    success_url = reverse_lazy('diary:index')

class DeleteView(generic.DeleteView):
    model = Day
    form_class = DayCreateForm
    success_url = reverse_lazy('diary:index')

class DetailView(generic.DetailView):
    model = Day
    


def index(request):
    context={
        'day_list':Day.objects.all(),
    }
    return render(request,'diary/day_list.html',context)

def add(request):
    #送信内容をもとにフォーム作成　POSTでないならば空フォーム idiom
    form= DayCreateForm(request.POST or None)
    
    #送信ボタン押下時、また入力内容に誤りがなければ
    if request.method =="POST" and form.is_valid():
        form.save() #dbに追加
        return redirect('diary:index') #二重投稿を防ぐ'diary:add'など
    
    #通常時のページアクセス、入力内容に誤りがあればページを再表示
    context={
        'form':form
    }
    return render(request,'diary/day_form.html', context)
    
    
    """context={
        'form':DayCreateForm()
    }
    return render(request,'diary/day_form.html', context)"""

def update(request,pk):
    #urlのpkをもとに、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    form = DayCreateForm(request.POST or None, instance=day)

    if request.method =='POST' and form.is_valid():
        form.save()
        return redirect('diary:index')

    context={
        'form':form
    }
    return render(request,'diary/day_form.html',context)

def delete(request,pk):
    day = get_object_or_404(Day, pk=pk)

    if request.method =='POST':
        day.delete()
        return redirect('diary:index')

    context={
        'day':day
    }
    return render(request,'diary/day_confirm_delete.html',context)

def detail(request,pk):
    day = get_object_or_404(Day, pk=pk)
    context={
        'day':day
    }
    return render(request,'diary/day_detail.html',context)